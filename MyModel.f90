Implicit None
Integer  Nx, Ny, Nz, time,iter,k2,pt,pt0
Integer i,j,k, ii0,ii1,jj0,jj1,kk0,kk1,Tmax, T 
Integer Cmax,NCT,areaoftumor
Real SKPT
parameter(nx=100,ny=100,nz=100,iter=1,SKPT=10000000,NCT=25,tmax=200000) 
	
real*8  oxygendensity(1:nx,1:ny,1:nz),energypercell(1:nx,1:ny,1:nz)
Real*8  energypersite(1:nx,1:ny,1:nz) 
real*8  allcells(1:nx,1:ny,1:nz),c(1:nx,1:ny,1:nz,1:NCT)
 
real*8  B0,B1,DecEng,DecOxy,C0,C1,In(NCT),Activity(tmax) 
Real  Volume1(tmax),Volume2(tmax),VolumeNCT(tmax),Volume(tmax),VolC(NCT)   

Real*8 R_m(NCT),bm,OD,CD,CR,SR  ,Mut_rate
Real*8 Delta1(NCT),Delta2(NCT),delta3(NCT),delta4(NCT),delta5(NCT),delta6(NCT),DelEng1
Real*8 Od1,Od2,Od3,Od4,Od5,Od6  
Real*8 r1,r2,r3,O0 
open(1,file='Growth.txt') 
open(2,file='ShapeCell.txt')   
open(3,file='ShapeEng.txt') 
Mut_rate=0.000d+0  
bm=8.0000            ! tsh of mitosis is 8     
R_m(1)=0.01d+0         ! rate of mitosis is 0.1
OD=0.1d+0  ! nutrient diffusion c  is 0.283 
CD=0.001d+0     ! diffusion of tumor cells becouse of pressure  0.01
cr=0.1d+0     ! nutrient consumption  0.1
DecEng=0.003d+0
DecOxy=0.0001d+0
cmax=Nx*Ny*Nz*0.4
B0=0.001d+0
B1=0
O0=B0/DecOxy 
Do pt=2,NCT
R_m(pt)=R_m(pt-1)*1.01d+0
end do 
  			      
Do t=1,tmax
Activity(t)=0
Volume(t)=0
Volume1(t)=0
Volume2(t)=0
VolumeNCT(t)=0
End Do  

 do k2=1,iter 

	print*, k2

         do i=0,1200*k2
				call random_number(r1)
				call random_number(r2)
				call random_number(r3)
         end do
         do i=1,nx 
           do j=1,ny 
			 do k=1,nz  
				do pt=1,NCT
				C(i,j,k,pt)=0
				end do
                energypersite(i,j,k)=0.0 
                oxygendensity(i,j,k)=O0
                energypercell(i,j,k)=0.0
                allcells(i,j,k)=0  
              end do
		    end do 
	     end do
		 
         do i=Nx/2-1,Nx/2+1 
           do j=Ny/2-1,Ny/2+1 
			 do k=Nz/2-1,Nz/2+1 
				  energypersite(i,j,k)=0.25d+0	   
				  energypercell(i,j,k)=5.d+0
				  do pt=1,NCT
				  C(i,j,k,pt)=0.002d+0
				  end do     
              end do
		    end do 
	     end do

 
do time=1,tmax
	SR=B0  !+0.00000000*CellNumber  
     ! print*,time 

	   do t=1,nx*ny*nz
			call random_number(r1)
			call random_number(r2)
			call random_number(r3) 

			i=nx*r1+1
			j=ny*r2+1
			k=nz*r3+1 
		!	print*, i,j,k 
			if(i==nx) then 
				ii1=nx
				else 
				ii1=i+1
				end if
				 
				if(i==1) then 
				ii0=1
				else 
				ii0=i-1
				end if 

				if(j==1) then 
				jj0=1
				else 
				jj0=j-1
				end if
				   
				if(j==ny) then 
				jj1=ny
				else 
				jj1=j+1
				end if 

				if(k==1) then 
				kk0=1
				else 
				kk0=k-1
				end if
				   
				if(k==nz) then 
				kk1=nz
				else 
				kk1=k+1
				end if
					 
		   allcells(i,j,k)=0 
		   do pt=1,NCT 				  		   
	       allcells(i,j,k)=allcells(i,j,k)+C(i,j,k,pt)
		   end do
	 	! print*,   allcells(i,j,k) !, C(i,j,k,5) !=allcells(i,j,k) 

Od1= oxygendensity(i,j,k)-oxygendensity(ii1,j,k) 	   
Od2= oxygendensity(i,j,k)-oxygendensity(ii0,j,k) 
Od3= oxygendensity(i,j,k)-oxygendensity(i,jj1,k) 
Od4= oxygendensity(i,j,k)-oxygendensity(i,jj0,k) 
Od5= oxygendensity(i,j,k)-oxygendensity(i,j,kk1) 
Od6= oxygendensity(i,j,k)-oxygendensity(i,j,kk0) 

!OU=SR
!if((oxygendensity(i,j,k)+OU)>1) then
!OU=0
!oxygendensity(i,j,k)=1
!end if 
oxygendensity(i,j,k)=oxygendensity(i,j,k)+SR 

oxygendensity(i,j,k)=oxygendensity(i,j,k)*(1.0d+0-DecOxy-cr*allcells(i,j,k))-OD*(Od1+Od2+Od3+Od4+Od5+Od6)  
oxygendensity(ii1,j,k)=oxygendensity(ii1,j,k)+OD*Od1   
oxygendensity(ii0,j,k)=oxygendensity(ii0,j,k)+OD*Od2
oxygendensity(i,jj1,k)=oxygendensity(i,jj1,k)+OD*Od3
oxygendensity(i,jj0,k)=oxygendensity(i,jj0,k)+OD*Od4
oxygendensity(i,j,kk1)=oxygendensity(i,j,kk1)+OD*Od5
oxygendensity(i,j,kk0)=oxygendensity(i,j,kk0)+OD*Od6



		  
if(allcells(i,j,k)>1.0d+0/SKPT) then
!print*, allcells(i,j,k)

energypersite(i,j,k)=energypersite(i,j,k)*(1.-DecEng)+ allcells(i,j,k)*oxygendensity(i,j,k) !*0.5d+0 
energypercell(i,j,k)=energypersite(i,j,k)/allcells(i,j,k)
if(energypercell(i,j,k)>bm) then

 C1=allcells(i,j,k)
  Do pt0=1,100 
  !
 ! C0=0
 ! do pt=1,NCT
 ! In(pt)= C(i,j,k,pt)*R_m(pt)*(1.)*(1.-allcells(i,j,k)) 
 ! C0=C0+In(pt)
 ! end do 

  C0=0
  do pt=1,NCT 
   In(pt)=C(i,j,k,pt)*R_m(pt)*(1.)*(1.-C1) 
   C0=C0+In(pt)
  end do
  C1=C0+allcells(i,j,k) 
  !Print*,pt0, C1, C0  
  end do 
  
   

   C(i,j,k,1)=C(i,j,k,1)+ In(1) !*(1.-Mut_rate) 
   Activity(time)=Activity(time)+In(1) 
  do pt=2,NCT 
   C(i,j,k,pt)=C(i,j,k,pt)+ In(pt) !*(1.-Mut_rate)+In(pt-1)* Mut_rate 
   Activity(time)=Activity(time)+In(pt)
end do

		   allcells(i,j,k)=0 
		   do pt=1,NCT 				  		   
	       allcells(i,j,k)=allcells(i,j,k)+ C(i,j,k,pt)
		   end do
		   ! print*, allcells(i,j,k)
		   energypercell(i,j,k)=energypersite(i,j,k)/(Allcells(i,j,k))
 
Do pt=1,NCT 
	Delta1(pt)=(floor(CD*C(i,j,k,pt)*SKPT)-floor(CD*C(ii1,j,k,pt)*SKPT))/(CD*SKPT) 
	Delta2(pt)=(floor(CD*C(i,j,k,pt)*SKPT)-floor(CD*C(ii0,j,k,pt)*SKPT))/(CD*SKPT) 
	Delta3(pt)=(floor(CD*C(i,j,k,pt)*SKPT)-floor(CD*C(i,jj1,k,pt)*SKPT))/(CD*SKPT) 
	Delta4(pt)=(floor(CD*C(i,j,k,pt)*SKPT)-floor(CD*C(i,jj0,k,pt)*SKPT))/(CD*SKPT) 
	Delta5(pt)=(floor(CD*C(i,j,k,pt)*SKPT)-floor(CD*C(i,j,kk1,pt)*SKPT))/(CD*SKPT) 
	Delta6(pt)=(floor(CD*C(i,j,k,pt)*SKPT)-floor(CD*C(i,j,kk0,pt)*SKPT))/(CD*SKPT) 
End Do 
 DelEng1=energypercell(i,j,k) 

Do pt=1,NCT 

C(i,j,k,pt)=C(i,j,k,pt)-CD*(Delta1(pt)+Delta2(pt)+Delta3(pt)+Delta4(pt)&
&+Delta5(pt)+Delta6(pt))
energypersite(i,j,k)=energypersite(i,j,k)-CD*(Delta1(pt)+Delta2(pt)+&
&Delta3(pt)+Delta4(pt)+Delta5(pt)+Delta6(pt))*DelEng1

C(ii1,j,k,pt)=C(ii1,j,k,pt)+CD*Delta1(pt)
energypersite(ii1,j,k)= energypersite(ii1,j,k)+CD*Delta1(pt)*DelEng1

C(ii0,j,k,pt)=C(ii0,j,k,pt)+CD*Delta2(pt)
energypersite(ii0,j,k)= energypersite(ii0,j,k)+CD*Delta2(pt)*DelEng1

C(i,jj1,k,pt)=C(i,jj1,k,pt)+CD*Delta3(pt)
energypersite(i,jj1,k)= energypersite(i,jj1,k)+CD*Delta3(pt)*DelEng1

C(i,jj0,k,pt)=C(i,jj0,k,pt)+CD*Delta4(pt)
energypersite(i,jj0,k)= energypersite(i,jj0,k)+CD*Delta4(pt)*DelEng1

C(i,j,kk1,pt)=C(i,j,kk1,pt)+CD*Delta5(pt)
energypersite(i,j,kk1)= energypersite(i,j,kk1)+CD*Delta5(pt)*DelEng1

C(i,j,kk0,pt)=C(i,j,kk0,pt)+CD*Delta6(pt)
energypersite(i,j,kk0)= energypersite(i,j,kk0)+CD*Delta6(pt)*DelEng1
 
End Do 
                  
 	  !		   allcells(i,j,k)=0 
!		   do pt=1,5				  		   
!	       allcells(i,j,k)=allcells(i,j,k)+ C(i,j,k,pt)
!		   end do
!    energypercell(i,j,k)=energypersite(i,j,k)/(Allcells(i,j,k))

		   end if	
		  end if




 
   end do  ! site selection  do 
 areaoftumor=0

 Do pt=1,NCT 
		 VolC(pt)=0
		 end do
 Do i=1,nx 							  
   Do j=1,ny
     Do k=1,Nz 
	 allcells(i,j,k)=0
	   Do pt=1,NCT 				  		   
	       Volume(time)=Volume(time)+ C(i,j,k,pt)
		   allcells(i,j,k)=allcells(i,j,k)+C(i,j,k,pt) 
		   VolC(pt)=VolC(pt)+C(i,j,k,pt) 
	   End Do
 
	   Volume1(time)=Volume1(time)+ C(i,j,k,1)
	   Volume2(time)=Volume2(time)+ C(i,j,k,10)
	   VolumeNCT(time)=VolumeNCT(time)+ C(i,j,k,NCT)



	  !  print*, allcells(i,j,k)  
           if(allcells(i,j,k)>1.d+0/SKPT) then 
           areaoftumor=areaoftumor+1
         end if
      End Do
    End Do
  End Do 

  Write(1,*) time/3.6,Volume(time)/1000,3.6*Activity(time)/1000 
  Write(*,*) time/3.6,Volume(time)/1000,Volume2(time)/1000,VolumeNCT(time)/1000 
 	   if(areaoftumor>cmax) goto 7
	   end do  !  time evel do 
7   continue
end do   ! iter


Do i=1,nx 							  
   Do j=1,ny
  !  Do k=1, nz 
   Write(2,*) i,j, allcells(i,j,nz/2) !  ,energypercell(i,j, nz/2) !,oxygendensity(i,j, nz/2)
   Write(3,*) i,j,energypercell(i,j, nz/2)  ,oxygendensity(i,j, nz/2)  
    End Do
  End Do 
 ! end do 
 
end
